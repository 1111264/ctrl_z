<head>
    <script src="vendor/jquery-handsontable-master/jquery-handsontable-master/lib/jquery.min.js"></script>
    <script src="vendor/jquery-handsontable-master/jquery-handsontable-master/jquery.handsontable.js"></script>
    <link rel="stylesheet" media="screen" href="vendor/jquery-handsontable-master/jquery-handsontable-master/jquery.handsontable.css">
    <link rel="stylesheet" media="screen" href="css/main.css">
</head>
<body>
<div id="example"></div>
<script>
    var data = [
    ["", "Maserati", "Mazda", "Mercedes", "Mini", "Mitsubishi"],
    ["2009", 0, 2941, 4303, 354, 5814],
    ["2010", 5, 2905, 2867, 412, 5284],
    ["2011", 4, 2517, 4822, 552, 6127],
    ["2012", 2, 2422, 5399, 776, 4151]
    ];

    $('#example').handsontable({
    data: data,
    minRows: 5,
    minCols: 6,
    minSpareRows: 1,
    autoWrapRow: true,
    colHeaders: true,
    contextMenu: true
    });

    $('.ver').html($('#example').data('handsontable').version);
</script>
</body>